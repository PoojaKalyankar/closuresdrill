const cacheFunction = require('../cacheFunction.cjs');

function cb(elements) {
  console.log("invoked function");
  //found the additon of elements less than 10
  const results = elements.reduce((accumulator, currentValue) => ((currentValue < 10) ? (accumulator + currentValue) : (accumulator)));
  return results;
}
const results = (cacheFunction(cb));
console.log(results([5, 8, 15, 16]));
console.log(results([5, 8, 15, 16]));
console.log(results([5, 8, 15, 16]));
console.log(results([]));