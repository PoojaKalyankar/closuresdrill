const limitFunctionCallCount = require('../limitFunctionCallCount.cjs');
function cb() {
    return "Iam invoke Function";
}
let n = 3;
const limitAdd = limitFunctionCallCount(cb, n);
//if we get function only we call otherwise we just console the string
if (typeof limitAdd === 'function') {
    console.log(limitAdd());
    console.log(limitAdd());
    console.log(limitAdd());
    console.log(limitAdd());
} else {
    console.log(limitAdd);
}