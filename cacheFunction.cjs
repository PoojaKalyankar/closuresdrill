function cacheFunction(cb) {
    const cache = {};
    return function (elements) {
        const string1 = JSON.stringify(elements);
        //checking if elements array is  not empty 
        if (string1.length > 0 && elements.length > 0) {
            if (!(string1 in cache)) {
                cache[string1] = cb(elements);
                return cache[string1];
            } else {
                return cache[string1];
            }
        } else {
            return "Mention Input";
        }
    }
}

module.exports = cacheFunction;