function counterFactory() {
    let counter = '0';
    //error handling to check if counter is number or not
    if (isNaN(Number(counter))) {
        counter = 0;
    } else {
        counter = Number(counter);
    }
    
    function increment() {
     counter = counter + 1;
     return counter;
    }
    function decrement() {
    counter = counter - 1;
     return counter;
    }
       return {increment,decrement};

}



module.exports = counterFactory;